package com.app.sochcast.utils

import java.io.File

class Functions {

    fun change_sec_to_time(file_duration: Long): String? {
        val second = file_duration / 1000 % 60
        val minute = file_duration / (1000 * 60) % 60
        return String.format("%02d:%02d", minute, second)
    }

    fun make_directry(path: String?) {
        val dir = File(path)
        if (!dir.exists()) {
            dir.mkdir()
        }
    }
}