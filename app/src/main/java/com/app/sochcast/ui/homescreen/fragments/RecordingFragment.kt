package com.app.sochcast.ui.homescreen.fragments

import android.Manifest
import android.app.Dialog
import android.content.ContextWrapper
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.app.sochcast.R
import com.app.sochcast.databinding.FragmentRecordingBinding
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class RecordingFragment : Fragment() {
    var binding: FragmentRecordingBinding? = null
    var mediaRecorder: MediaRecorder? = null
    var audioFileName: String? = null
    var customAudioName: String? = null
    var customDialog: Dialog? = null
    private var pauseOffset: Long = 0
    private var isRecording = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_recording, container, false)
        val rootView: View = binding!!.getRoot()
        if (isMicroPhonePresent) {
            microPhonePermission
        }
        binding!!.record.setOnClickListener(View.OnClickListener {
            recordAudio()
            binding!!.timer.setBase(SystemClock.elapsedRealtime() - pauseOffset)
            binding!!.timer.start()
            isRecording = true
            binding!!.record.setEnabled(false)
            binding!!.save.setVisibility(View.VISIBLE)
            binding!!.cancel.setVisibility(View.VISIBLE)
        })
        binding!!.cancel.setOnClickListener(View.OnClickListener {
            binding!!.save.setVisibility(View.GONE)
            binding!!.cancel.setVisibility(View.GONE)
            binding!!.timer.setBase(SystemClock.elapsedRealtime())
            pauseOffset = 0
            binding!!.timer.stop()
            isRecording = false
            binding!!.record.setEnabled(true)
            val contextWrapper = ContextWrapper(requireActivity().applicationContext)
            val musicDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_MUSIC)
            val removeFile = File(musicDirectory, "$audioFileName.mp3")
            if (removeFile.exists()) {
                if (removeFile.delete()) {
                    Log.e("DELETE", "DONE")
                }
            }
        })
        binding!!.save.setOnClickListener(View.OnClickListener {
            customDialog = Dialog(requireActivity())
            val sheetView: View =
                requireActivity().layoutInflater.inflate(R.layout.custom_dialog_filename, null)
            customDialog!!.setContentView(sheetView)
            val customAudioFileName = sheetView.findViewById<EditText>(R.id.audioName)
            val saveAudioFile = sheetView.findViewById<Button>(R.id.saveAudio)
            saveAudioFile.setOnClickListener {
                customAudioName = customAudioFileName.text.toString()
                binding!!.save.setVisibility(View.GONE)
                binding!!.cancel.setVisibility(View.GONE)
                binding!!.timer.setBase(SystemClock.elapsedRealtime())
                pauseOffset = 0
                binding!!.timer.stop()
                isRecording = false
                binding!!.record.setEnabled(true)
                stopRecord()
                customDialog!!.setCancelable(false)

                customDialog!!.dismiss()
            }
            customDialog!!.show()
            val window = customDialog!!.window
            window!!.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
        })
        return rootView
    }

    fun recordAudio() {
        try {
            mediaRecorder = MediaRecorder()
            mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
            mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            mediaRecorder!!.setOutputFile(recordingFilePath)
            mediaRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            mediaRecorder!!.prepare()
            mediaRecorder!!.start()
            Toast.makeText(activity, "Recorded Started", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Toast.makeText(activity, e.message, Toast.LENGTH_SHORT).show()
            Log.e("MEDIA ERROR", e.message!!)
        }
    }

    fun stopRecord() {
        mediaRecorder!!.stop()
        mediaRecorder!!.release()
        mediaRecorder = null
        val contextWrapper = ContextWrapper(requireActivity().applicationContext)
        val musicDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_MUSIC)
        val from = File(musicDirectory, "$audioFileName.mp3")
        val to = File(musicDirectory, "$customAudioName.mp3")
        if (from.exists()) {
            from.renameTo(to)
        }
        Toast.makeText(activity, "Recorder Stopped", Toast.LENGTH_SHORT).show()
    }

    private val isMicroPhonePresent: Boolean
        private get() = requireContext().packageManager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE)
    private val microPhonePermission: Unit
        private get() {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.RECORD_AUDIO
                ) == PackageManager.PERMISSION_DENIED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.RECORD_AUDIO),
                    MICROPHONE_PERMISSION
                )
            }
        }
    private val recordingFilePath: String
        private get() {
            val dateFormat = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            audioFileName = "AUDIO" + "_" + dateFormat + "_" + "DEMO"
            val contextWrapper = ContextWrapper(requireActivity().applicationContext)
            val musicDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_MUSIC)
            val file = File(musicDirectory, "$audioFileName.mp3")
            return file.path
        }

    companion object {
        private const val MICROPHONE_PERMISSION = 1
    }
}