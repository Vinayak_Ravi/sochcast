package com.app.sochcast.ui.homescreen.fragments

import android.content.ContextWrapper
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sochcast.R
import com.app.sochcast.databinding.FragmentAudioListBinding
import com.app.sochcast.ui.homescreen.fragments.adapter.AudioListAdapter
import com.app.sochcast.ui.homescreen.fragments.model.AudioList
import java.io.IOException
import java.util.*

class AudioListFragment : Fragment() {
    private val audioLists: ArrayList<AudioList> = ArrayList<AudioList>()
    var binding: FragmentAudioListBinding? = null
    var audioListAdapter: AudioListAdapter? = null
    var mediaPlayer: MediaPlayer? = null
    var tempPos = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_audio_list, container, false)
        val rootView: View = binding!!.getRoot()
        audioListAdapter = AudioListAdapter(audioLists, requireContext(), object :
            AudioListAdapter.OnItemClickListener {
            override fun onItemClick(position: Int) {
                try {
                    if (position == tempPos && mediaPlayer != null) {
                        mediaPlayer!!.stop()
                        mediaPlayer!!.release()
                        mediaPlayer = null
                    } else {
                        if (mediaPlayer != null) {
                            mediaPlayer!!.stop()
                            mediaPlayer!!.release()
                            mediaPlayer = null
                            val filePath: String? = audioLists[position].audio_path
                            mediaPlayer = MediaPlayer()
                            mediaPlayer!!.setDataSource(filePath)
                            mediaPlayer!!.prepare()
                            mediaPlayer!!.start()
                            tempPos = position
                        } else {
                            tempPos = position
                            val filePath: String? = audioLists[position].audio_path
                            mediaPlayer = MediaPlayer()
                            mediaPlayer!!.setDataSource(filePath)
                            mediaPlayer!!.prepare()
                            mediaPlayer!!.start()
                            Toast.makeText(activity, "Record is Playing", Toast.LENGTH_SHORT).show()
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        })
        binding!!.list.setLayoutManager(
            LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
        )
        binding!!.list.setAdapter(audioListAdapter)
        allAudioPath
        return rootView
    }

    val allAudioPath: Unit
        get() {
            val contextWrapper = ContextWrapper(requireActivity().applicationContext)
            val musicDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_MUSIC)
            val files = musicDirectory!!.listFiles()
            for (i in files.indices) {
                val file = files[i]
                val item = AudioList()
                item.audio_path = file.absolutePath
                item.audio_duration_ms = getfileduration(Uri.parse(file.absolutePath))
                item.audio_file_name = file.name
                val createdDated = Date(file.lastModified())
                item.audio_created_date = createdDated.toString()
                Log.d("resp", "" + item.audio_duration_ms)
                if (item.audio_duration_ms > 5000) {
                    item.audio_time = change_sec_to_time(item.audio_duration_ms)
                    audioLists.add(item)
                }
            }
            audioListAdapter!!.setData(audioLists)
        }

    // get the audio file duration that is store in our directory
    fun getfileduration(uri: Uri?): Long {
        try {
            val mmr = MediaMetadataRetriever()
            mmr.setDataSource(activity, uri)
            val durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
            val file_duration = durationStr!!.toInt()
            return file_duration.toLong()
        } catch (e: Exception) {
        }
        return 0
    }

    fun change_sec_to_time(file_duration: Long): String? {
        val second = file_duration / 1000 % 60
        val minute = file_duration / (1000 * 60) % 60
        return String.format("%02d:%02d", minute, second)
    }}