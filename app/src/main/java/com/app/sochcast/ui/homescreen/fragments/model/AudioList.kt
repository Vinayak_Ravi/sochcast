package com.app.sochcast.ui.homescreen.fragments.model

class AudioList {
    var audio_path: String? = null
    var audio_time: String? = null
    var audio_duration_ms: Long = 0
    var audio_created_date: String? = null
    var audio_file_name: String? = null
}