package com.app.sochcast.ui.homescreen.fragments.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.sochcast.R
import com.app.sochcast.ui.homescreen.fragments.model.AudioList

class AudioListAdapter(
    audioLists: ArrayList<AudioList>,
    ctx: Context,
    listener: Any
) :
    RecyclerView.Adapter<AudioListAdapter.ViewHolder>() {
    private var audioLists: ArrayList<AudioList>?
    private val context: Context
    private val listener: OnItemClickListener
    fun setData(audioLists: ArrayList<AudioList>) {
        this.audioLists = audioLists
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_audio_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val audioList: AudioList = audioLists!![position]
        holder.audio_file_name.setText(audioList.audio_file_name)
        holder.created_date.setText(audioList.audio_created_date)
        holder.playBtn.setOnClickListener { listener.onItemClick(position) }
    }

    override fun getItemCount(): Int {
        return if (audioLists != null) {
            audioLists!!.size
        } else {
            0
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var audio_file_name: TextView
        var created_date: TextView
        var playBtn: ImageView

        init {
            audio_file_name = view.findViewById(R.id.audio_file_name)
            created_date = view.findViewById(R.id.created_date)
            playBtn = view.findViewById(R.id.play)
        }
    }

    init {
        this.audioLists = audioLists
        context = ctx
        this.listener = listener as OnItemClickListener
    }
}
