package com.app.sochcast.ui.homescreen

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.NavigationUI.setupWithNavController
import com.app.sochcast.R
import com.app.sochcast.databinding.ActivityHomeScreenBinding

class HomeScreen : AppCompatActivity() {
    var binding: ActivityHomeScreenBinding? = null
    var navController: NavController? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home_screen)
        navController = findNavController(this, R.id.navHostFragment)
        setupWithNavController(binding!!.botttomNavView, navController!!)
        binding!!.fab.setOnClickListener(View.OnClickListener {
            findNavController(findViewById(R.id.navHostFragment)).navigate(
                R.id.recordingFragment
            )
        })
    }
}